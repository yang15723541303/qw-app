// pages/message/child/forward/index.js
const app = getApp()
import { ajaxMessagesArticlesShares } from '../../../../api/api.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    height: app.globalData.height,
    createdModal: { pageNum: 1, pageSize: 10 },
    list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    ajaxMessagesArticlesShares(this.data.createdModal).done(res=>{
      this.setData({list:res.data.list})
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})