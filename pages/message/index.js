// pages/message/index.js

const app = getApp()

import { login } from '../../utils/validate/login-validate.js'
let loginValidate = new login()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    height: app.globalData.height,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },


  // 转发 , 点赞，评论消息操作
  _bindMessage(e){
    let arr = ['./child/forward/index', './child/lick/index', './child/collect/index', './child/comment/index','./child/system/index']
    let index = e.currentTarget.dataset.ind
    
    if (index != 4 && !loginValidate.onLoginValidate()) { //登录验证未通过
      return
    }

    wx.navigateTo({
      url: arr[index],
    })

  },



  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})