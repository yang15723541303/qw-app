// pages/search/index.js

const app = getApp()
import { ajaxArticlesSearch} from '../../api/api.js'

import regeneratorRuntime from '../../regenerator-runtime/runtime.js'


Page({

  /**
   * 页面的初始数据
   */
  data: {
    height: app.globalData.height,
    pageType:false ,  // false 时不显示搜索数据， true时 是点击过搜索之后
    hotList:[
      { title: '试管婴儿' },
      { title: '整形美容' },
      { title: '不孕不育' },
      { title:'肺腺癌'},
    ],
    keyword:null,
    modelPage:{pageNum:1,pageSize:10},
    list: {
      status: false,  // 是否需要清除之前的内容，true清除，false反之
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },




  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },


  _input(e){
    this.setData({ 'modelPage.pageNum': 1,keyword:e.detail.value})
    // this._search();
  },

  /**
   * 搜索
   * */ 
  _search(){
    let data = { ...this.data.modelPage, keyword: this.data.keyword}
    ajaxArticlesSearch(data).done(res=>{
      console.log(res.data.list)
      this.setData({ pageType: true, 'list.arr':  res.data.list, 'list.status':true})
      console.log(list)
    })

  },


  /**
   * 清除关键字
   * */ 

  _clear(){
    this.setData({ keyword: null, pageType:false })
  },


  /**
   * 点击热门搜索的提示按钮
   * */ 
  _enterBtn(e){
    this.setData({ 'modelPage.pageNum': 1, keyword: e.currentTarget.dataset.keyword })
    this._search();
  },

  /**
   * 主动触发搜索
   * */ 
  _searchBtn(){
    this._search();
  },









})