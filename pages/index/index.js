//index.js
//获取应用实例
const app = getApp()

import { ajaxArticlesRecommend} from '../../api/api.js'


Page({
  data: {
    height: app.globalData.height,
    imgWidth: 0,
    imgHeight: 0,
    list:{
      status:false,  // 是否需要清除之前的内容，true清除，false反之
    },
    requestData:{
      pageNum:1,pageSize:10
    },
    bottomTips:'',
    hasNextPage:true,   // 是否有下一页，有下一页上拉加载更多
    //app.globalData.height * 2 + 20,   
  },


  onLoad: function() {
  
    this.findAll()

  },

  onShow(){
   
  },

  onshareappmessage(){
    
  },


  /**
   * 查询推荐的文章
   * 
   * */ 

  findAll(){
    ajaxArticlesRecommend(this.data.requestData).done(res => {
      console.log(res)
      this.setData({
        'list.arr': res.data.list,//[...res.data.list, ...res.data.list, ...res.data.list, ...res.data.list, ...res.data.list, ...res.data.list],
        hasNextPage: res.data.hasNextPage,
        bottomTips: res.data.hasNextPage ? '正在加载中……' : '没有更多了喔……'
      })
    })
  },


  onReachBottom: function () {
      console.log('上拉加载更多……')
    if (!this.data.hasNextPage){
      console.log('没有更多了')
      return
    }
    this.setData({ pageNum: this.data.requestData.pageNum ++ })
    this.findAll()



  },



})