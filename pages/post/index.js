const app = getApp()
import CONFIG from '../..//config/config.js'
// import qiniuUploader from '../../utils/qiniu.js'
const qiniuUploader = require("../../utils/qiniu.js");
import { ajaxarticlesCategories, ajaxarticles, ajaxQiniucloudToken} from '../../api/api.js'
// pages/post/index.js
import { login } from '../../utils/validate/login-validate.js'
let loginValidate = new login()


Page({

  /**
   * 页面的初始数据
   */
  data: {
    qiniuToken:null,
    height: app.globalData.height,
    requestData:{  //上传一片文章的参数
      imgUrl: [],
      content:null,
      title:null,
      categoryId:null,
    },
    classList:[],
    array:[],
    index:null  // 当前选中的category，配合array使用 picker
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    //查询所有类目，便于选择标签 
    ajaxarticlesCategories().done(res => {
      let arr = res.data.map(item=>{return item.name})
      this.setData({ classList: res.data, array:arr})
    })

    /**
     * 获取七牛token
     * 此处设计不当，不应该每次加载的时候请求token，
     * 应该根据七牛token过期之后重新请求
    */
    ajaxQiniucloudToken().done(res=>{
      this.setData({ qiniuToken: res.data.upToken})
      console.log(res)
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /** picker选择 */
  bindPickerChange(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value,
      'requestData.categoryId': this.data.classList[e.detail.value].id
    })
  },
  
  /**
   * 发布文章
   * */ 
  _up(){
    console.log(this.data.requestData)
    if (!this.data.requestData.title){
      wx.showToast({
        title: '请输入标题',
        icon:'none'
      })
      return
    } else if (!this.data.requestData.categoryId){
      wx.showToast({
        title: '请选择一个标签',
        icon: 'none'
      })
      return
    } else if (!this.data.requestData.content){
      wx.showToast({
        title: '内容不能为空',
        icon: 'none'
      })
      return
    }
    if (!loginValidate.onLoginValidate()) { //登录验证未通过
      return
    }

    ajaxarticles(this.data.requestData).done(res=>{
      wx.showToast({
        title: '发布成功',
        duration:500
      })
      this.setData({
        requestData: {  //上传成功之后就将上传的信息清空
          imgUrl: [],
          content: null,
          title: null,
          categoryId: null,
        }, index:null})
        wx.switchTab({
          url:'/pages/index/index'
        })
    })
  },

  /**
   * 删除选中的某张图片
   * */ 
  _removeImage(e){
    let index = e.currentTarget.dataset.ind
    let arr = this.data.requestData.imgUrl.splice(index, 1)
    arr.splice(index,1)
    // debugger
    this.setData({'requestData.imgUrl':arr})
  },

  _inputTitle(e) {
    this.setData({ 'requestData.title': e.detail.value })
    // this._search();
  },

  _inputContent(e) {
    this.setData({ 'requestData.content': e.detail.value })
    // this._search();
  },

  _back(){
    debugger
    wx.navigateBack({
      delta:2
    })
  },
  /**
   * 
   * 选择图片
   * 
   * */ 
  _chooseImage(){
    let _this = this
    wx.chooseImage({
      count:4,
      success: function(res) {
        console.log('选择图片成功了',res)
        // 上传图片
        res.tempFilePaths.forEach(item=>{
          
          wx.getImageInfo({
            src: item,
            success(r) {
              let width = r.width
              let height = r.height
              let type = r.type

              let qiniuToken = _this.data.qiniuToken;  //七牛token
              let file = r.path;  //上传的文件
              const key = 'qw-images' + parseInt(Math.random() * 10000) + new Date().getTime().toString() + '.' + type; //上传的文件名

              let config = {
                useCdnDomain: true,   //表示是否使用 cdn 加速域名，为布尔值，true 表示使用，默认为 false。
                region: 'images'     // 根据具体提示修改上传地区,当为 null 或 undefined 时，自动分析上传域名区域
              };
              let putExtra = {
                fname: null,  //文件原文件名
                params: {}, //用来放置自定义变量
                mimeType: null  //用来限制上传文件类型，为 null 时表示不对文件类型限制；限制类型放到数组里： ["image/png", "image/jpeg", "image/gif"]
              };
              
              console.log(r)
              qiniuUploader.upload(file, 
              (fileres) => { 
                console.log(fileres)
                let obj = { width: width, height: height, url: fileres.imageURL}
                let arr = [obj, ..._this.data.requestData.imgUrl]
                _this.setData({'requestData.imgUrl':arr})
                
              },
              (error)=>{  
                console.log('错误',error)
              },
               { // 参数设置  地区代码 token domain 和直传的链接 注意七牛四个不同地域的链接不一样，我使用的是华南地区
                 region: 'SCN',
                 uptoken: qiniuToken,
                 uploadURL: CONFIG.uploadURL,  //上传地址
                 domain: CONFIG.domain,
              });
             
            }
          })
        })


      },
    })
  },
})