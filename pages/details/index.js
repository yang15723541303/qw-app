// pages/details/index.js
const app = getApp()
import { ajaxUser, ajaxArticlesDetails, ajaxCommentsReplies, ajaxComments, ajaxArticlesRecommendLike} from '../../api/api.js'

import regeneratorRuntime from '../../regenerator-runtime/runtime.js'



Page({

  /**
   * 页面的初始数据
   */
  data: {
    height: app.globalData.height,
    modalRe:{
      pageNum:1,
      pageSize:10,
      articleId:null
    },
    lickHttpModal:{
      articleId: null, pageNum: 1, pageSize:10
    },
    userInfo:{}, //用户信息
    article:{},  // 文章信息
    comments:[],   // 评论列表
    likeArtic:[
      { userInfo: {}, article: {}, comments:{}}
    ],  //相似文章列表
    bottomTips: '',
    hasNextPage: true,   // 是否有下一页，有下一页上拉加载更多
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad:  async function (options) {
    let userInfo, article, comments
    // 查询用户信息
  //  await  ajaxUser(options.userid).done(res=>{
  //    userInfo = res.data
  //     // this.setData({ 'likeArtic[0].userInfo':res.data})
  //   })
    this.setData({ 'modalRe.articleId': options.id, 'lickHttpModal.articleId': options.id})
    // 查询文章信息
    await  ajaxArticlesDetails(options.id).done(res=>{
      /** 需要优化，为了赶时间 ，mark一下 展开字段的添加 */ 

      let prop = 750 / res.data.article.imgUrl[0].width
      res.data.article.imgUrl[0].width = res.data.article.imgUrl[0].width * prop
      res.data.article.imgUrl[0].height = res.data.article.imgUrl[0].height * prop

      let resdata = res.data.article
      let title = resdata.title || ''
      let content = resdata.content || ''
      let titleLenght = resdata.title.toString().length || 0
      let contentLength = resdata.content ? resdata.content.toString().length : 0
      let articleLength = titleLenght + contentLength
      resdata.articleLength = articleLength
      resdata.hasOpen = articleLength > 40 ? true : false  //是否有展开字段
      resdata.isOpen = false   //是否已经展开
      resdata.openTitle = title + '  |  ' + content 
      article = res.data
      console.log(article)
    }) 
    //查询评论列表
    // await ajaxCommentsReplies(this.data.modalRe).done(res=>{
    //   comments = res.data.list.map(itm => { return itm.comment})  
    //   // this.setData({ 'likeArtic[0].comments':res.data.list})
    // })
    this.setData({ 'likeArtic[0]': article})
  
    this._findLike()
    


  },

//查询相似文章
_findLike(){
  // this.setData({'lickHttpModal.articleId':id})
  ajaxArticlesRecommendLike(this.data.lickHttpModal).done(res=>{
    let likeArtic = JSON.parse(JSON.stringify(this.data.likeArtic))  
    let newLikeArtic = res.data.list
    newLikeArtic = newLikeArtic.map(item=>{
        let prop = 750 / item.article.imgUrl[0].width
        item.article.imgUrl[0].width = item.article.imgUrl[0].width * prop
        item.article.imgUrl[0].height = item.article.imgUrl[0].height * prop

      let title = item.article.title || ''
      let content = item.article.content || ''
      let titleLenght = item.article.title.toString().length || 0
      let contentLength = item.article.content ? item.article.content.toString().length  : 0
      let articleLength = titleLenght + contentLength
      item.article.articleLength = articleLength
      item.article.hasOpen = articleLength > 40 ? true : false  //是否有展开字段
      item.article.isOpen = false   //是否已经展开
      item.article.openTitle = title + '  |  ' +content 
      return item
    })
    
    likeArtic = [...likeArtic, ...newLikeArtic]
    
    this.setData({
      likeArtic: likeArtic, hasNextPage: res.data.hasNextPage,
      bottomTips: res.data.hasNextPage ? '正在加载中……' : '没有更多了喔……'} )
    console.log('likeArtic=======',this.data.likeArtic)
    // console.log(this.data.likeArtic)
  })
},


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('上拉加载更多……')
    if (!this.data.hasNextPage) {
      console.log('没有更多了')
      return
    }
    this.setData({ pageNum: this.data.lickHttpModal.pageNum++ })
    this._findLike()
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

 




})