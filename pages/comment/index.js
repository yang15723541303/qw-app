// pages/comment/index.js
const app = getApp()
import {
  ajaxCommentsReplies, ajaxComments, ajaxReplies
} from '../../api/api.js'

import { login } from '../../utils/validate/login-validate.js'

let loginValidate = new login()

import regeneratorRuntime from '../../regenerator-runtime/runtime.js'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isX: app.globalData.isX,
    height: app.globalData.height,
    modalRe: {
      pageNum: 1,
      pageSize: 10,
      articleId: null
    },
    hasNextPage: true, //是否有下一页，判断能否滚动
    commentsList: {},
    type:0,  //  评论和回复类型，0 是评论 1 是回复
    authorId:null,
    commentData:{
      commentId:null,
      replyId:null,
      replyType:null,
      toUserId:null,
      toUserNickname:null,
      toUserAvatar:null,
      content:null,
    }

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      'modalRe.articleId': options.id, authorId:options.aid
    })
    
    /**
     * 查询所有评论或者回复
     * */ 
    ajaxCommentsReplies(this.data.modalRe).done(res => {
      this.setData({
        commentsList: res.data.list,
        hasNextPage: res.data.hasNextPage
      })
    })



  },

  _resetObj (){
    let obj = [...this.data.commentData]
    Object.keys(obj).forEach(key => obj[key] = null);
    this.setData({ commentData:obj})
  },
  /**
   * 添加一个评论
   * fromUserId,fromUserNickname,fromUserAvatar 已经写进接口不用重复写
   * */ 
  saveComment(){
    if (!this.data.commentData.content){
      wx.showToast({
        title: '请输入评论内容',
        icon:"none"
      })
      return
    }
    if (!loginValidate.onLoginValidate()) { //登录验证未通过
      return
    }
    let data = { articleId: this.data.modalRe.articleId, authorId: this.data.authorId, content: this.data.commentData.content}

    ajaxComments(data).done(res=>{
      wx.showToast({
        title: '评论成功',
        icon:"none"
      })
      this._resetObj()
    })
  },

  _input(e){
    this.setData({ 'commentData.content': e.detail.value})
  },

  /**
   * 回复输入框input事件
   * */ 
  _inputReply(e){
    this.setData({ 'commentData.content': e.detail.value })
    if (!e.detail.value && !this.data.commentData.content){
      this.setData({ type:0})
    }
    // this.setData({ 'commentData.content': e.detail.value })
  },

  /**
   * 用户触发回复 按钮
   * 给commentData赋值
   * */ 
  _replyTap(e){
    console.log(e)
    
    this.setData({ type: 1,
     'commentData.replyType': e.currentTarget.dataset.ctype,
      'commentData.commentId': e.currentTarget.dataset.id,
      'commentData.replyId': e.currentTarget.dataset.ctype == 2 ? e.currentTarget.dataset.obj.id : null,
      'commentData.toUserId': e.currentTarget.dataset.obj.fromUserId,
      'commentData.toUserNickname': e.currentTarget.dataset.obj.fromUserNickname,
      'commentData.toUserAvatar':e.currentTarget.dataset.obj.fromUserAvatar,
     })
     console.log(this.data)
  },

  /**
   * 得到焦点
   * */ 
  _focus() {
    loginValidate.onLoginValidate()
  },

  /**
   * 新增一个回复
   * */ 
  saveReply(){
    if (!this.data.commentData.content) {
      wx.showToast({
        title: '请输入回复内容',
        icon:"none"
      })
      return
    }
    if (!loginValidate.onLoginValidate()) { //登录验证未通过
      return
    }

    ajaxReplies(this.data.commentData).done(res=>{
      wx.showToast({
        title: '回复成功',
        icon:'none'
      })
      this.setData({ type: 0 })
      this._resetObj()
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    if (!this.data.hasNextPage){
      return
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})