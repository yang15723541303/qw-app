// pages/find/index.js
const app = getApp()

import {
  ajaxarticlesCategories,
  ajaxArticlesCategory
} from '../../api/api.js'
import regeneratorRuntime from '../../regenerator-runtime/runtime.js'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    height: app.globalData.height,
    list: { // 传给瀑布流的参数
      status: false, // 是否需要清除之前的内容，true清除，false反之
      arr: []
    },
    //分类类目
    classList: [],
    modalHttp: {
      id: 0,
      pageNum: 1,
      pageSize: 10
    },
    bottomTips: '',
    hasNextPage: true, // 是否有下一页，有下一页上拉加载更多
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function(options) {
    // 查询分类列表
    await ajaxarticlesCategories().done(res => {
      this.setData({
        classList: res.data,
        'modalHttp.id': res.data[0].id
      })
    })
    let data = this.data.modalHttp
    this._createdHttp(data)

  },


  /**
   * 请求分类数据,查询分类下面的文章
   * */
  _createdHttp(data, status = false) {
    ajaxArticlesCategory(data).done(res => {
      this.setData({
        'list.arr': res.data.list,
        'list.status': status,
        hasNextPage: res.data.hasNextPage,
        bottomTips: res.data.hasNextPage ? '正在加载中……' : '没有更多了喔……'
      })
    })
  },

  /**
   * 分类切换
   * */

  _classTap(e) {

    let id = e.currentTarget.dataset.id
    if (id == this.data.modalHttp.id) {
      return
    }
    this.setData({
      modalHttp: {
        id: id,
        pageNum: 1,
        pageSize: 10
      }
    })
    let data = this.data.modalHttp
    this._createdHttp(data, true)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    console.log('上拉加载更多……')
    if (!this.data.hasNextPage) {
      console.log('没有更多了')
      return
    }
    this.setData({
      pageNum: this.data.modalHttp.pageNum++
    })
    this._createdHttp(this.data.modalHttp)



  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})