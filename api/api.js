import _ajax from './config.js'
const app = getApp()

/*登录 */ 

export const ajaxLogin = data => _ajax.post(`users/wxLogin?code=${data.code}`,data)


export const ajaxArticlesRecommend = data => _ajax.get(`articles/recommend?pageNum=${data.pageNum}&pageSize=${data.pageSize}`)


/* 查询所有类目 */
export const ajaxarticlesCategories = data => _ajax.get(`articles/categories`)


/* 根据id查询文章列表  */ 
export const ajaxArticlesCategory = data => _ajax.get(`articles/category/${data.id}?pageNum=${data.pageNum}&pageSize=${data.pageSize}`)


/* 查询一篇文章详情 */
export const ajaxArticlesDetails = data => _ajax.get(`articles/${data}?userId=${app.store.$state.userInfo.userId || '999'}`)

/* 查询相似文章推荐 */
export const ajaxArticlesRecommendLike = data => _ajax.get(`articles/recommend/${data.articleId}?userId=${app.store.$state.userInfo.userId || '999'}&pageNum=${data.pageNum}&pageSize=${data.pageSize}`)

/* 查询一个用户的信息 */ 
export const  ajaxUser = data => _ajax.get(`users/${data}?userId=${app.store.$state.userInfo.userId || '999'}`)


/* c\查询文章评论和回复 */ 
export const ajaxCommentsReplies = data => _ajax.get(`comments/replies?articleId=${data.articleId}&pageNum=${data.pageNum}&pageSize=${data.pageSize}`)


/* 添加一个用户评论 */ 
export const ajaxComments = data => _ajax.post(`comments?authorId=${data.authorId}`, data, false, { fromUserId: app.store.$state.userInfo.userId || '999', fromUserNickname: app.store.$state.userInfo.nickName || '十一', fromUserAvatar: app.store.$state.userInfo.avatarUrl || 'http://image20.it168.com/201509_800x800/2305/e338a45b30c7824f.png' })



/* 添加一个回复 */ 
export const ajaxReplies = data => _ajax.post(`replies`, data, false, { fromUserId: app.store.$state.userInfo.userId || '999', fromUserNickname: app.store.$state.userInfo.nickName || '十一', fromUserAvatar: app.store.$state.userInfo.avatarUrl || 'http://image20.it168.com/201509_800x800/2305/e338a45b30c7824f.png' })




/* 关键字搜索文章 */ 
export const ajaxArticlesSearch = data => _ajax.get(`articles/search?keyword=${data.keyword}&pageNum=${data.pageNum}&pageSize=${data.pageSize}`)


/* 新增一篇文章  */ 
export const ajaxarticles = data => _ajax.post(`articles`, data, true,
  { userId: app.store.$state.userInfo.userId || '999', nickname: app.store.$state.userInfo.nickName || '十一', avatar: app.store.$state.userInfo.avatarUrl || 'http://image20.it168.com/201509_800x800/2305/e338a45b30c7824f.png' })


/* 查询用户收藏的文章列表  */ 
export const ajaxArticlesSaved = data => _ajax.get(`articles/saved/${data.id}?pageNum=${data.pageNum}&pageSize=${data.pageSize}`)

/* 根据用户的id查询发布的文章 */
export const ajaxArticlesPublished = data => _ajax.get(`articles/published/${data.id}?pageNum=${data.pageNum}&pageSize=${data.pageSize}`)

/* 关注 */ 
export const ajaxUsersRelations = data => _ajax.post('users/relations', data, false, { followUserId: app.store.$state.userInfo.userId || '999'})

/* 取消关注 */ 
export const ajaxUsersRelationsDel = data => _ajax.get(`users/relations/?userId=${data.userId}&followUserId=${app.store.$state.userInfo.userId || '999'}`)


/* 喜欢一个文章 */ 
export const ajaxArticlesLike = data => _ajax.post(`articles/${data.id}/liked?authorId=${data.authorId}&userId=${app.store.$state.userInfo.userId || '999'}`)

/* 取消喜欢 */
export const ajaxDelArticlesLike = data => _ajax.post(`articles/${data.id}/liked?authorId=${data.authorId}&userId=${app.store.$state.userInfo.userId || '999'}`, {}, false, {}, 'DELETE')

/* 加入收藏*/
export const ajaxArticlesSave = data => _ajax.post(`articles/${data.id}/saved?authorId=${data.authorId}&userId=${app.store.$state.userInfo.userId || '999'}`)

/* 取消收藏 */ 
export const ajaxDelArticlesSave = data => _ajax.post(`articles/${data.id}/saved?authorId=${data.authorId}&userId=${app.store.$state.userInfo.userId || '999'}`, {}, false, {},'DELETE')




/* 获取期牛牛token */ 
export const ajaxQiniucloudToken = data => _ajax.get(`qiniucloud/oss/uptoken`)



/** ============================消息模块=============================== **/ 

/* 消息通知 - 喜欢了文章 */ 
export const ajaxMessagesArticlesLikes = data => _ajax.get(`messages/articles/liked?userId=${app.store.$state.userInfo.userId || '999'}&pageNum=${data.pageNum}&pageSize=${data.pageSize}`)

/* 消息通知 - 转发了文章  */ 
export const ajaxMessagesArticlesShares = data => _ajax.get(`messages/articles/shared?userId=${app.store.$state.userInfo.userId || '999'}&pageNum=${data.pageNum}&pageSize=${data.pageSize}`)

/* 消息通知 - 收藏了文章 */ 
export const ajaxMessagesArticlesSaves = data => _ajax.get(`messages/articles/saved?userId=${app.store.$state.userInfo.userId || '999'}&pageNum=${data.pageNum}&pageSize=${data.pageSize}`)

/* 消息通知 - 评论了文章 */ 
export const ajaxMessagesArticlesComments = data => _ajax.get(`messages/articles/commented?userId=${app.store.$state.userInfo.userId || '999'}&pageNum=${data.pageNum}&pageSize=${data.pageSize}`)


