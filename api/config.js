

import CONFIG from '../config/config.js'

var app_url = CONFIG.app_url
let primaryToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0bXBTZWNyZXRLZXkiOiI1ZTRiYTFmNDQyYzM4ZTA5IiwiZXhwIjoxNTU3Mzk3MTcyLCJ1c2VySWQiOjk5OX0.FD7BhE97STjN2roa4O9VrAswVx9ojkdbdglX4Lo0GZU'

const _ajax = function () {
}


// 调用 success ，之后后台接口code为200 （即成功）时，才会执行
Promise.prototype.success = function (fn) {
  typeof fn == 'function' && this.then(resp => {
    resp.code == '200' && fn(resp)
  })
  return this
}

// 调用done，发现code不为200，即执行toast
Promise.prototype.done = function (fn) {
  this.then(resp => {
    console.log(resp)
    if (typeof resp !='object'){
      // resp = "'"+resp+"'"
      // resp = JSON.parse(resp)
      console.log(resp)
      
    }
    resp.code != '200' && (wx.showToast({
      title: '网络错误！', icon: 'none'
    }))
    typeof fn == "function" && resp.code == "200" && fn(resp);
  })

  return this
}




_ajax.post = (url, data={}, toast = false, config = {},method="POST") => {
  
  data = Object.assign(data, config)
  
  let token = wx.getStorageSync('userInfo').jwtToken || primaryToken
  toast && (wx.showLoading({ title: '请稍等...', mask: 'trun' }))


  return new Promise((resolve, reject) => {
    wx.request({
      method: method,
      url: app_url + url,
      data: data,
      header: { "Content-Type": "application/json;charset=UTF-8", 'Authorization': token}, //
      success: function (res) {
        wx.hideLoading()
        console.log(res)
        
        if (res.data.code == 401 && res.data.description == "access_token can't be null") {

        } else {
          console.log('post的res回来了', res)
          resolve(res.data)
        }




      },
      error: function (e) {
        wx.hideLoading()
        reject('网络错误');
      }
    })

  })
}


_ajax.get = function (url, data, toast = false, config = {a:1}) {
  let token = wx.getStorageSync('userInfo').jwtToken || primaryToken
  toast && (wx.showLoading({ title: '请稍等...', mask: 'trun' }))
  return new Promise((resolve, reject) => {
    wx.request({
      method: 'GET',
      url: app_url + url,
      data: data,
      header: { "Content-Type": "application/json;charset=UTF-8", 'Authorization': token },
      success: function (res) {
        res = JSON.parse(JSON.stringify(res))
        resolve(res.data)
      },
      error: function (e) {
        console.log(e)
        reject('网络错误，请稍后重试！');
      }
    })

  })

}
export default _ajax