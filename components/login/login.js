// components/login/login.js
const app = getApp()
import { ajaxLogin} from '../../api/api.js'
Component({


  /**
   * 组件的属性列表
   * @params   {Booblean}  showType   组件显示状态  false不显示 true显示
   */
  properties: {
    showType:{
      type:Boolean,
      value:false
    }
  },

  /**
   * 组件的初始数据
   * @params   {Booblean}  authType   登录状态 ，false 未登录，true已登录
   * 
   */
  data: {
    loginShowType: app.store.loginShowType, 
    
  },

  /**
   * 组件的方法列表
   */
  methods: {
    shareCancel(e) {
      app.store.setState({ loginShowType:false})
      console.log(this.data)
    },


    /**  
           * 登录，先从缓存中取出用户信息，如果没有用户信息，再通过授权获取
           * 
           */
    onGotUserInfo(e) {
      console.log(e)
      let _this = this
      if (e.detail.errMsg =="getUserInfo:fail auth deny"){
        wx.showToast({
          title: '请授权微信登录',
          icon:'none',
          duration:1000
        })
      }else{

          // wx.login({
          //   success: function (res) {
        ajaxLogin({ code: wx.getStorageSync('userCode'), signature: e.detail.signature, encryptedData: e.detail.encryptedData, iv: e.detail.iv}).done(r=>{
                let a = Object.assign(r.data, e.detail.userInfo)
                console.log('用户信息',a)
                wx.setStorageSync('userInfo', a)
                wx.setStorageSync('userInfoServer', e.detail)
                app.store.setState({ loginShowType: false, userInfo: a})
                app.store.setState({ authType: true })
              })
            // }
          // })
        

       
      }
      console.log(app.store)

    },
  }
})
