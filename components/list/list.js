// components/list/list.js

// var leftList = new Array();//左侧集合
// var rightList = new Array();//右侧集合

import { login } from '../../utils/validate/login-validate.js'
let loginValidate = new login()

import { ajaxArticlesLike} from '../../api/api.js'

var leftHight = 0,
  rightHight = 0,
  itemWidth = 0,
  maxHeight = 0;

const app = getApp()
let windowsWidth = (app.globalData.windowsWidth * 0.5 ) - 30
Component({
  /**
   * 组件的属性列表
   */

  properties: {
    list: {
      type: Object,
      value: [],
    }
  },
  lifetimes: {
    attached() {
        leftHight = 0,
        rightHight = 0,
        itemWidth = 0,
        maxHeight = 0;
    },
  },
  observers: {
    'list': function(newVal, oldVal) {

      newVal.status && (this.setData({ rightList: [], leftList: [], rightHight: 0, leftHight:0}), rightHight = leftHight = 0  )


      let [liftArray,rightArray] = [[],[]]
      newVal.arr && newVal.arr.length && newVal.arr.forEach((item, index) => {
          
          // 图片放大 pro 倍数 
        let pro = 345 / 2 / item.imgUrl[0].width  
        item.imgUrl[0].width = (item.imgUrl[0].width * pro) || 375   
        item.imgUrl[0].height = (item.imgUrl[0].height * pro) || 375
        // isLick 接口暂未返回是否喜欢字段，前端模拟的，等接口返回之后删除
        item.isLick = false  
        
        if (this.data.leftHight <= this.data.rightHight) {
       
          liftArray.push(item)
          this.setData({ leftHight: this.data.leftHight += item.imgUrl[0].height + 20}) 
          console.log('leftHight=====', this.data.leftHight)
        } else {
          rightArray.push(item)
          // rightHight += item.imgUrl[0].height + 20
          this.setData({ rightHight: this.data.rightHight += item.imgUrl[0].height + 20 }) 

          console.log('rightHight', this.data.rightHight)
        }

      })
      
       this.setData({
         rightList: [...this.data.rightList,...rightArray] ,
         leftList: [...this.data.leftList,...liftArray]
        })
        
    },

  },
  /**
   * 组件的初始数据
   */
  data: {
    leftHight:0,
    rightHight:0,
    leftList: [],
    rightList: [],

  },

  /**
   * 组件的方法列表
   */
  methods: {
    _lick(e){ // 点击喜欢
      if (!loginValidate.onLoginValidate()) { //登录验证未通过
        return
      }


      let fromId = e.currentTarget.dataset.fromid
      let index = e.currentTarget.dataset.ind
      let item = e.currentTarget.dataset.fromid == 'left' ? 'leftList[' + index + '].isLick' : 'rightList[' + index +'].isLick'

      


      ajaxArticlesLike({ id: e.currentTarget.dataset.id,
        authorId: e.currentTarget.dataset.aid}).done(res=>{
        this.setData({ [item]:true})
      })
    }
  },

})