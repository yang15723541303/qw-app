// components/details/details.js
import {
  login
} from '../../utils/validate/login-validate.js'
import {
  ajaxUsersRelations,
  ajaxUsersRelationsDel,
  ajaxComments,
  ajaxArticlesSave,
  ajaxDelArticlesSave,
  ajaxDelArticlesLike,
  ajaxArticlesLike
} from '../../api/api.js'
let loginValidate = new login()
import regeneratorRuntime from '../../regenerator-runtime/runtime.js'
const app = getApp()
// let windowsWidth = (app.globalData.windowsWidth / 750) 
// debugger

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    list: Array
  },
  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached() {

      console.log(this.data.list)
    },
    moved() {},
    detached() {},
  },
  observers: {
    'list': function(newVal, oldVal) {
     
      console.log('list========', newVal)
    }
  },
  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

    /* 展开 and 收起 */
    _opne(e) {
      let index = e.currentTarget.dataset.ind
      let a = 'list[' + index + '].article.isOpen'
      this.setData({
        [a]: !this.data.list[index].article.isOpen
      })
      console.log(this.data.list[index].article.isOpen)
    },

    /**
     * 关注
     */
    async _follow(e) {
      let index = e.currentTarget.dataset.ind
      let userId = 'list[' + index + '].userInfo.id'
      let follow = 'list[' + index + '].userInfo.isFollow'

      if (!loginValidate.onLoginValidate()) { //登录验证未通过
        return
      }

      let content = "关注成功"
      if (this.data.list[index].userInfo.isFollow) { // 取消关注
        await ajaxUsersRelationsDel({
          userId: this.data.list[index].userInfo.id
        })
        content = "取消关注成功"
      } else { //关注
        await ajaxUsersRelations({
          userId: this.data.list[index].userInfo.id
        })
      }
      wx.showToast({
        title: content,
        icon: 'none',
        duration: 1000
      })
      this.setData({
        [follow]: !this.data.list[index].userInfo.isFollow
      })
    },

    /**
     * 评论框 得到焦点
     */
    _focus() {
      loginValidate.onLoginValidate()
    },

    /**
     * 点击收藏
     * */
    _collect(e) {
      if (!loginValidate.onLoginValidate()) { //登录验证未通过
        return
      }
      let index = e.currentTarget.dataset.ind
      let id = e.currentTarget.dataset.id
      let authorId = e.currentTarget.dataset.aid
      let isSaved = e.currentTarget.dataset.issaved
      let save = 'list[' + index + '].article.isSaved'
      if (isSaved) { // true已经收藏过了，取消收藏
        ajaxDelArticlesSave({
          id: e.currentTarget.dataset.id,
          authorId: authorId
        }).done(res => {
          this.setData({
            [save]: false
          })
        })
      } else {
        ajaxArticlesSave({
          id: e.currentTarget.dataset.id,
          authorId: authorId
        }).done(res => {
          this.setData({
            [save]: true
          })

        })
      }

    },


    /**
     * 喜欢文章
     * */
    _lick(e) {
      if (!loginValidate.onLoginValidate()) { //登录验证未通过
        return
      }
      let index = e.currentTarget.dataset.ind
      let id = e.currentTarget.dataset.id
      let authorId = e.currentTarget.dataset.aid
      let islick = e.currentTarget.dataset.islick
      let lick = 'list[' + index + '].article.isLiked'
      if (islick) { // true已经喜欢了，取消喜欢
        ajaxDelArticlesLike({
          id: e.currentTarget.dataset.id,
          authorId: authorId
        }).done(res => {
          this.setData({
            [lick]: false
          })
        })
      } else {
        ajaxArticlesLike({
          id: e.currentTarget.dataset.id,
          authorId: authorId
        }).done(res => {
          this.setData({
            [lick]: true
          })
        })
      }


    },

    /** 
     * 确认添加评论 
     * */
    endConfirm(e) {
      let val = e.detail.value
      let id = e.currentTarget.dataset.id
      let authorId = e.currentTarget.dataset.aid
      if (!val) {
        wx.showToast({
          title: '请输入评论内容',
          icon: "none"
        })
      }
      if (!loginValidate.onLoginValidate()) { //登录验证未通过
        return
      }
      let data = {
        articleId: id,
        authorId: authorId,
        content: val
      }
      ajaxComments(data).done(res => {
        wx.showToast({
          title: '评论成功',
          icon: "none"
        })
      })
    },

  }
})