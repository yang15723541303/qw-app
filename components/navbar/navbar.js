const app = getApp()


/**
 * 
 * @param  {String}   title           ''        标题参数
 * @param  {Boolean}  showCapsule     false      是否显示左上角 返回按钮，默认不显示
 * @param  {Boolean}  hasShadow       true       导航条下方是否存在阴影
 * @param  {Number}   showType        0           title位置显示别的内容  0 显示title 1 == 搜索框
 * @param  {Boolean}  isBack          false      true 时显示 X
 */


Component({
  properties: {

    title:{type:String,value:''},   // title
    showCapsule:{  
      type:Boolean,
      value:false ,   
    },
    hasShadow:{ 
      type:Boolean,
      value:true,
    },

    showType: {   
      type:Number,
      value:0  
    },
    isBack:{
      type:Boolean,
      value:false
    }

  },
  data: {
    height: 0,
    //默认值  默认显示左上角
    navbarData: {
      showCapsule: 1
    }
  },
  attached: function () {
    // 获取是否是通过分享进入的小程序
    this.setData({
      share: app.globalData.share
    })
    // 定义导航栏的高度   方便对齐
    this.setData({
      height: app.globalData.height
    })
  },
  methods: {
    // 返回上一页面
    _navback() {
      wx.navigateBack()
    },
    //返回到首页
    _backhome() {
      wx.switchTab({
        url: '/pages/index/index',
      })
    }
  }

}) 