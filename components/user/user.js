const app = getApp()
import {
  meCover
} from '../../utils/adapter/me-adapter.js'
import {
  ajaxArticlesSaved, ajaxArticlesPublished, ajaxUser
} from '../../api/api.js'

import _debounce from '../../utils/debounce.js'
Component({
  attached() {
  },


  /**
   * 组件的属性列表
   */
  properties: {
    userInfo: {
      type: Object
    },
    userId:{
      type:Number,
      value:0
    },
    user: {
      type: String,
      value: 'ta'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    currentTab: 0,
    saveCreatedModel: { //  查询收藏
      id: null,
      pageNum: 1,
      pageSize: 10
    },
    pushCreatedModel:{  // 查询发布文章
      id:null,
      pageNum:1,
      pageSize:10
    },
    newUserInfo:{},
    saveList: { //收藏文章list
      status: false, // 是否需要清除之前的内容，true清除，false反之
    },
    saveTips:'',
    saveHasNextPage:true,
    pushList: { //收藏文章list
      status: false, // 是否需要清除之前的内容，true清除，false反之
    },
    pushTtips:'',
    pushHasNextPage:true
  },
  observers: {
    'userInfo': function(newValue) {
      console.log('userInfog更新了', newValue)
      
      if (newValue.hasOwnProperty('userId') || newValue.hasOwnProperty('id')) {
        this.setData({
          'saveCreatedModel.id': newValue.userId || newValue.id,
          'pushCreatedModel.id': newValue.userId || newValue.id,
        })
      }
    
    },
    'saveCreatedModel.id': function(newValue) {
      let data = this.data.saveCreatedModel
      this.findAllSaved(data,true)
    },
    'pushCreatedModel.id': function (newValue) {
      let data = this.data.pushCreatedModel
      this.findAllPush(data,true)
    },
    /** 代码很乱，逻辑混乱重复，需要优化 mark */ 
    'userId':function(newVal){
      newVal && this.findUser(newVal)
    }
  },


  /**
   * 组件的方法列表
   */
  methods: {
    selectLogin() {
      app.store.setState({
        loginShowType: true
      })
      console.log(app.store)
    },


    swichNav: function(e) {
      console.log(e)
      var cur = e.target.dataset.current;
      if (this.data.currentTab == cur) {
        return false;
      } else {
        var currentTab = cur;
        this.setData({
          currentTab: currentTab
        })
      }
    },

    // 滚动切换标签样式
    switchTab: function(e) {
      var currentTab = e.detail.current;
      this.setData({
        currentTab: currentTab,
      });
    },

    // 查询所有收藏的文章
    findAllSaved(data, status=false) {
      ajaxArticlesSaved(data).done(res => {
        console.log('查询到所有的收藏文章,')
        this.setData({
          'saveList.arr': res.data.list,
          'saveList.status': status,
          saveHasNextPage: res.data.hasNextPage,
          saveTips: res.data.hasNextPage ? '正在加载中……' : '没有更多了喔……'
        })
      })
    },
    findUser(id){
      ajaxUser(id).done(res => {
        console.log('获取到用户的信息', res.data)
        this.setData({ newUserInfo: res.data })
      })
    },
    // 查询所有发布的文章
    findAllPush(data, status = false){
      ajaxArticlesPublished(data).done(res => {
        console.log('查询到所有的发布文章,')
        this.setData({
          'pushList.arr': res.data.list,
          'pushList.status': status,
          pushHasNextPage: res.data.hasNextPage,
          pushTips: res.data.hasNextPage ? '正在加载中……' : '没有更多了喔……'
        })
      })
    },

    /**
     * 收藏下拉触底事件
     * */ 
    loewrSave: _debounce(function () {
      if (!this.data.saveHasNextPage  ) {
        return
      }
      let _this = this
      _this.setData({
        'saveCreatedModel.pageNum': ++_this.data.saveCreatedModel.pageNum,
      })
      _this.findAllSaved(_this.data.saveCreatedModel, false)
    }, 1000),


    /**
     * 经历文章下拉触底
     * */ 
    loewrPush: _debounce(function () {
      if (!this.data.saveHasNextPage) {
        return
      }
      let _this = this
      _this.setData({
        'pushCreatedModel.pageNum': ++_this.data.pushCreatedModel.pageNum,
      })
      _this.findAllPush(_this.data.saveCreatedModel, false)
    }, 1000),

  }
})