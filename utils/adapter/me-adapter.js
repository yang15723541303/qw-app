const localModal = function (avatar, nickName, followCnt = 0, followedCnt = 0, expValue = 0, creditValue=0){
  this.avatar = avatar
  this.nickname = nickName
  this.followCnt = followCnt
  this.followedCnt = followedCnt
  this.expValue = expValue
  this.creditValue = creditValue
  return this
}


export const meCover = function(data) {
  return new localModal(data.avatarUrl, data.nickName  )
}