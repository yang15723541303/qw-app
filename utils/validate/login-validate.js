/**
 * 在需要登录的地方调用
 * */ 
let app = getApp()


export class login {
  constructor(){
    this.result = false
  }

  onLoginValidate(){
    if (!app.store.$state.authType){  //未登录状态
      app.store.setState({ loginShowType: true })
    }else{
      this.result = true
    }

    return this.result
  }

}

