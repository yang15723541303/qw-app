/**
 * 2019年03月26日21:55:26
 * author ：YangZhengPing
 * 小程序 config 配置
 * http://api.iqwdc.cn:8080/api/wx/v1/qiniucloud/oss/uptoken
 */

const _config = {
  //app_url: 'https://nei.netease.com/api/apimock/de61f58002933e17d7210c46f6a9587e/api/wx/v1/',
  icon_url:'./',
  app_url:'https://api.iqwdc.com/api/wx/v1/',
  // app_url:'https://9bb8f3a6.ngrok.io/api/wx/v1/',
  uploadURL:'https://up.qbox.me',
  domain:'http://pr9u9qoeo.bkt.clouddn.com'

}
export default _config