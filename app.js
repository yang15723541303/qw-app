//app.js
const Store = require('/utils/store.js');

let store = new Store({
  /**
   * @params   {Boolean}   loginShowType   登录组件显示状态 false不显示
   * @params   {Boolean}   authType        登录状态 true已登录，false未登录
   * 
  */
  
  state: {
    loginShowType: false,
    authType:false,
    userInfo:wx.getStorageSync("userInfo") || {}
  }

})

App({

  onLaunch: function (options) {

    let _this = this
    wx.getSetting({   // 授权查询
      success: function (res) {
        if (res.authSetting['scope.userInfo'] && wx.getStorageSync('userInfo')) { // 已经授权，直接执行登录接口
          store.setState({ authType:true})
    
        } else { //没有读取到授权信息，将authType 置为false状态
          store.setState({ authType: false })
          console.log('没有授权')
        }
      }
    })
    wx.login({
      success: function (res) {
        console.log('获取到了code',res)
       wx.setStorageSync('userCode', res.code)
      }
    })

    wx.getSystemInfo({
      success(res) {
        console.log(res.statusBarHeight)// 获取状态栏的高度
        console.log(res)
        if (res.model.search('iPhone X') != -1) {
          _this.globalData.isX = true
        }
        _this.globalData.height = res.statusBarHeight
        _this.globalData.windowsWidth = res.windowWidth
        _this.globalData.height = res.statusBarHeight
        // _this.setData({
        //   headheight: res.statusBarHeight + 44
        // })
      }
    })
    // 判断是否由分享进入小程序
    if (options.scene == 1007 || options.scene == 1008) {
      this.globalData.share = true
    } else {
      this.globalData.share = false
    };
    //获取设备顶部窗口的高度（不同设备窗口高度不一样，根据这个来设置自定义导航栏的高度）
    //这个最初我是在组件中获取，但是出现了一个问题，当第一次进入小程序时导航栏会把
   
    // wx.getSystemInfo({
    //   success: (res) => {
    //     this.globalData.height = res.statusBarHeight
    //     this.globalData.windowsWidth = res.windowWidth
    //   }
    // })
  },
  store:store,
 /**
  * 设置监听器
  */

  setWatch(data,watch){
   
    Object.keys(watch).forEach(item=>{
      this.observe(data,item,watch[item])
    })
  },

  observe(obj,key,watchFn){
    var val = obj[key]; // 给该属性设默认值
    Object.defineProperty(obj, key, {
      configurable: true,
      enumerable: true,
      set: function (value) {
        val = value;
        watchFun(value, val); // 赋值(set)时，调用对应函数
      },
      get: function () {
        return val;
      }
    })
  
  },


  globalData: {
    share: false,  // 分享默认为false
    height: 0,   //  得到 手机导航栏高度
    windowsWidth:0,
    isX:false
  }
})